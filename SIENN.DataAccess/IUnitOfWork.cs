﻿using SIENN.DataAccess.Repositories;
using System;

namespace SIENN.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository Categories { get; }
        IProductRepository Products { get; }
        ITypeRepository Types { get; }
        IUnitRepository Units { get; }
        int Commit();
    }
}

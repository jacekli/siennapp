﻿namespace SIENN.DataAccess.Entities
{
    public class Type
    {
        public int TypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}

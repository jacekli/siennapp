﻿namespace SIENN.DataAccess.Entities
{
    public class Unit
    {
        public int UnitId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SIENN.DataAccess.Entities
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime DeliveryDate { get; set; }
  
        public int TypeId { get; set; }
        public Type Type { get; set; }

        public int UnitId { get; set; }
        public Unit Unit { get; set; }

        public ICollection<ProductCategory> ProductCategories { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace SIENN.DataAccess.Entities
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public ICollection<ProductCategory> ProductCategories { get; set; }
    }
}

﻿using SIENN.DataAccess.Entities;
using System.Collections.Generic;

namespace SIENN.DataAccess.Repositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Product GetWithData(int id);
        IEnumerable<Product> GetAvailable(int start, int count);
        IEnumerable<Product> GetBy(int? typeId, int? unitId, int[] categoryId);
    }
}

﻿using SIENN.DataAccess.Entities;

namespace SIENN.DataAccess.Repositories
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}

﻿using SIENN.DataAccess.Entities;

namespace SIENN.DataAccess.Repositories
{
    public interface ITypeRepository : IGenericRepository<Type>
    {
    }
}

﻿using SIENN.DataAccess.Entities;

namespace SIENN.DataAccess.Repositories
{
    public interface IUnitRepository : IGenericRepository<Unit>
    {
    }
}

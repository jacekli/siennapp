SELECT  *, AVG(Products.Price) as 'CategoryAveragePrice'
FROM Categories 
	INNER JOIN ProductCategories ON categories.ID = ProductCategories.CategoryID
	INNER JOIN Products ON Products.ID = ProductCategories.ProductID
	inner join (
		select CategoryId as 'catId', count(Products.ID) as 'abc' from Products inner join ProductCategories on Products.ID = ProductCategories.ProductID where Products.IsAvailable = 1 group by CategoryID
	) as iop on Categories.ID = catId
		group by Categories.ID
		order by CategoryAveragePrice desc


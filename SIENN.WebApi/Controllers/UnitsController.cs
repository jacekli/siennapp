﻿using Microsoft.AspNetCore.Mvc;
using SIENN.DataAccess;
using SIENN.DataAccess.Entities;

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class UnitsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public UnitsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_unitOfWork.Units.GetAll());
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var unit = _unitOfWork.Units.Get(id);

            if (unit == null)
                return NotFound();

            return Ok(unit);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]Unit unit)
        {
            if (unit.UnitId != 0)
                return BadRequest();

            _unitOfWork.Units.Add(unit);
            _unitOfWork.Commit();

            return Ok();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Unit unitUpdated)
        {
            var unit = _unitOfWork.Units.Get(id);
            if (id != unit.UnitId)
                return BadRequest();

            unit.Code = unitUpdated.Code;
            unit.Description = unitUpdated.Description;

            _unitOfWork.Commit();

            return Ok();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var unit = _unitOfWork.Units.Get(id);
            if (unit == null)
                  return BadRequest();
            _unitOfWork.Units.Remove(unit);
            _unitOfWork.Commit();

            return Ok();
        }
    }
}

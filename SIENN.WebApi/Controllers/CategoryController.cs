﻿using Microsoft.AspNetCore.Mvc;
using SIENN.DataAccess;
using SIENN.DataAccess.Entities;

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_unitOfWork.Categories.GetAll());
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var item = _unitOfWork.Categories.Get(id);

            if (item == null)
                return NotFound();

            return Ok(item);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]Category item)
        {
            if (item == null || item.CategoryId != 0)
                return BadRequest();

            _unitOfWork.Categories.Add(item);
            _unitOfWork.Commit();

            return Ok();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Unit itemUpdate)
        {
            if (itemUpdate == null)
                return BadRequest();

            var item = _unitOfWork.Categories.Get(id);
            if (id != item.CategoryId)
                return BadRequest();

            item.Code = itemUpdate.Code;
            item.Description = itemUpdate.Description;

            _unitOfWork.Commit();

            return Ok();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _unitOfWork.Categories.Get(id);
            if (item == null)
                return BadRequest();
            _unitOfWork.Categories.Remove(item);
            _unitOfWork.Commit();

            return Ok();
        }
    }
}

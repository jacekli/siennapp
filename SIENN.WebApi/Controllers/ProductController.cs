﻿using Microsoft.AspNetCore.Mvc;
using SIENN.DataAccess;
using SIENN.DataAccess.Entities;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Product")]
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Product
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_unitOfWork.Products.GetAll());
        }

        // GET: api/Product/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            var product = _unitOfWork.Products.Get(id);

            if (product == null)
                return NotFound();

            return Ok(product);
        }

        [HttpGet("Available")]
        public IActionResult GetAvailable([FromQuery]int? page, [FromQuery]int? count)
        {
            int pageValue = page ?? 1;
            int countValue = count ?? 10;
            int start = (pageValue - 1) * countValue;

            var products = _unitOfWork.Products.GetAvailable(start, countValue);

            if (products == null)
                return NotFound();

            return Ok(products);
        }

        [HttpGet("Search")]
        public IActionResult GetBy([FromQuery]int? typeId, [FromQuery]int? unitId, [FromQuery] int[] categoryId)
        {
            var products = _unitOfWork.Products.GetBy(typeId, unitId, categoryId);

            return Ok(products);
        }

        [HttpGet("Info/{id}")]
        public IActionResult GetProductInfo(int id)
        {
            var product = _unitOfWork.Products.GetWithData(id);
            if (product == null)
                return NotFound();

            var result = new {
                ProductDescription = $"({product.Code}) {product.Description}",
                Price = $"{product.Price:N2} zł",
                IsAvailable = product.IsAvailable ? "Dostępny" : "Niedostępny",
                DeliveryDate = $"{product.DeliveryDate:dd.MM.yyyy}",
                CategoriesCount = product.ProductCategories.Count,
                Type = $"({product.Type.Code}) {product.Type.Description}",
                Unit = $"({product.Unit.Code}) {product.Unit.Description}"
            };

            return Ok(result);
        }

        // POST: api/Product
        [HttpPost]
        public IActionResult Post([FromBody]Product product)
        {
            if (product.ProductId != 0)
                return BadRequest();

            _unitOfWork.Products.Add(product);
            _unitOfWork.Commit();

            return Ok();
        }
        
        // PUT: api/Product/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Product productUpdated)
        {
            var product = _unitOfWork.Products.Get(id);
            if (id != product.ProductId)
                return BadRequest();

            product.Code = productUpdated.Code;
            product.Description = productUpdated.Description;

            _unitOfWork.Commit();

            return Ok();
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var product = _unitOfWork.Products.Get(id);
            if (product == null)
                return BadRequest();
            _unitOfWork.Products.Remove(product);
            _unitOfWork.Commit();

            return Ok();
        }
    }
}

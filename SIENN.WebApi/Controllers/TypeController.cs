﻿using Microsoft.AspNetCore.Mvc;
using SIENN.DataAccess;
using SIENN.DataAccess.Entities;

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TypeController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public TypeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_unitOfWork.Types.GetAll());
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var item = _unitOfWork.Types.Get(id);

            if (item == null)
                return NotFound();

            return Ok(item);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]Type item)
        {
            if (item.TypeId != 0)
                return BadRequest();

            _unitOfWork.Types.Add(item);
            _unitOfWork.Commit();

            return Ok();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Unit itemUpdate)
        {
            var item = _unitOfWork.Types.Get(id);
            if (id != item.TypeId)
                return BadRequest();

            item.Code = itemUpdate.Code;
            item.Description = itemUpdate.Description;

            _unitOfWork.Commit();

            return Ok();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _unitOfWork.Types.Get(id);
            if (item == null)
                return BadRequest();
            _unitOfWork.Types.Remove(item);
            _unitOfWork.Commit();

            return Ok();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using SIENN.DataAccess.Entities;
using SIENN.DataAccess.Repositories;

namespace SIENN.DbAccess.Repositories
{
    public class UnitRepository : GenericRepository<Unit>, IUnitRepository
    {
        public UnitRepository(DbContext context) : base(context) {}
    }
}

﻿using Microsoft.EntityFrameworkCore;
using SIENN.DataAccess.Entities;
using SIENN.DataAccess.Repositories;

namespace SIENN.DbAccess.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbContext context) : base(context)
        {
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using SIENN.DataAccess.Entities;
using SIENN.DataAccess.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace SIENN.DbAccess.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly SiennContext _context;
        private readonly DbSet<Product> _products;

        public ProductRepository(DbContext context) : base(context)
        {
            _context = (SiennContext)context;
            _products = context.Set<Product>();
        }

        public IEnumerable<Product> GetAvailable(int start, int count)
        {
            return _products.Where(p => p.IsAvailable).Skip(start).Take(count).ToList();
        }

        public Product GetWithData(int id)
        {
            return _products.
                Include(p => p.Type).
                Include(p => p.Unit).
                Include(p => p.ProductCategories).
                Single(p => p.ProductId == id);
        }

        public IEnumerable<Product> GetBy(int? typeId, int? unitId, int[] categoryId)
        {
            var query = _context.Products.Join(_context.ProductCategory,
                    p => p.ProductId,
                    pc => pc.ProductId,
                    (p, pc) => new { Prod = p, ProdCat = pc });

            if (typeId.HasValue)
                query = query.Where(p => p.Prod.TypeId == typeId.Value);

            if (unitId.HasValue)
                query = query.Where(p => p.Prod.UnitId == unitId.Value);

            if (categoryId != null)
                foreach (int id in categoryId)
                    query = query.Where(x => x.ProdCat.CategoryId == id);

            return query.Select(x => x.Prod).ToList();
        }
    }
}

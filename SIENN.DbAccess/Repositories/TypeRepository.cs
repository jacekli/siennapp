﻿using Microsoft.EntityFrameworkCore;
using SIENN.DataAccess.Entities;
using SIENN.DataAccess.Repositories;

namespace SIENN.DbAccess.Repositories
{
    public class TypeRepository : GenericRepository<Type>, ITypeRepository
    {
        public TypeRepository(DbContext context) : base(context)
        {
        }
    }
}

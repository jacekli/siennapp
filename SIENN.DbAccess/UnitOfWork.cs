﻿using SIENN.DataAccess;
using SIENN.DataAccess.Repositories;
using SIENN.DbAccess.Repositories;

namespace SIENN.DbAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SiennContext _context;

        public UnitOfWork(SiennContext context)
        {
            _context = context;
            Categories = new CategoryRepository(context);
            Products = new ProductRepository(context);
            Types = new TypeRepository(context);
            Units = new UnitRepository(context);
        }

        public ICategoryRepository Categories { get; }
        public IProductRepository Products { get; }
        public ITypeRepository Types { get; }
        public IUnitRepository Units { get; }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
